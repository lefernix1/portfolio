const needList = [
  {
    title: "Dépannage informatique",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in egestas orci. Maecenas lacus urna, fermentum vel magna et, malesuada viverra magna.",
    image: "/contents/help-computer.jpg",
  },
  {
    title: "Développement web",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in egestas orci. Maecenas lacus urna, fermentum vel magna et, malesuada viverra magna.",
    image: "/contents/web-dev.jpg",
  },
  {
    title: "Collaboration",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in egestas orci. Maecenas lacus urna, fermentum vel magna et.",
    image: "/contents/collaboration.jpg",
  },
];

export default [
  { label: "Besoin", content: needList },
  { label: "Services", content: needList },
  { label: "Finaliser", content: needList },
];
