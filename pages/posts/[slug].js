import React from "react";
import { Heading, Container, Badge, Link, Box, Code } from "@chakra-ui/react";
import { ChevronRightIcon } from "@chakra-ui/icons";
import NextLink from "next/link";
import P from "../../components/paragraph";
import Layout from "../../components/layouts/article";
import { pathOr, prop } from "ramda";
import moment from "moment";

import { getPosts, getPostDetails } from "../../services";
import Author from "../../components/author";

const Post = ({ post }) => {
  const getContentFragment = (index, text, obj, type) => {
    let modifiedText = text;

    if (obj) {
      if (obj.bold) {
        modifiedText = <b key={index}>{text}</b>;
      }

      if (obj.italic) {
        modifiedText = <em key={index}>{text}</em>;
      }

      if (obj.underline) {
        modifiedText = <u key={index}>{text}</u>;
      }
    }

    switch (type) {
      case "heading-three":
        return (
          <h3 key={index} className='text-xl font-semibold mb-4'>
            {modifiedText.map((item, i) => (
              <React.Fragment key={i}>{item}</React.Fragment>
            ))}
          </h3>
        );
      case "paragraph":
        return (
          <p key={index} className='mb-8'>
            {modifiedText.map((item, i) => (
              <React.Fragment key={i}>{item}</React.Fragment>
            ))}
          </p>
        );
      case "heading-four":
        return (
          <h4 key={index} className='text-md font-semibold mb-4'>
            {modifiedText.map((item, i) => (
              <React.Fragment key={i}>{item}</React.Fragment>
            ))}
          </h4>
        );
      case "code-block":
        return (
          <h4 key={index} className='text-md font-semibold mb-4'>
            {modifiedText.map((item, i) => (
              <Code key={i}>{item}</Code>
            ))}
          </h4>
        );
      case "image":
        return (
          <img
            key={index}
            alt={obj.title}
            height={obj.height}
            width={obj.width}
            src={obj.src}
          />
        );
      default:
        return modifiedText;
    }
  };
  return (
    <Layout title='Inkdrop'>
      <Container>
        <Box>
          <NextLink href='/posts'>
            <Link>Blog</Link>
          </NextLink>
          <span>
            {" "}
            <ChevronRightIcon />{" "}
          </span>
          <Heading display='inline-block' as='h3' fontSize={20} mb={4}>
            {prop("title", post)}{" "}
          </Heading>
        </Box>
        <P>
          {post.content.raw.children.map((typeObj, index) => {
            const children = typeObj.children.map((item, itemindex) =>
              getContentFragment(itemindex, item.text, item)
            );
            return getContentFragment(index, children, typeObj, typeObj.type);
          })}
        </P>
      </Container>
      <Author
        author={post.author}
        createdAt={post.createdAt}
        updatedAt={post.updatedAt}
      />
    </Layout>
  );
};

export default Post;

export async function getStaticProps({ params }) {
  const data = (await getPostDetails(params.slug)) || [];

  return {
    props: { post: data },
  };
}

export async function getStaticPaths() {
  const posts = (await getPosts()) || [];

  return {
    paths: posts.map(({ node: { slug } }) => ({ params: { slug } })),
    fallback: false,
  };
}
