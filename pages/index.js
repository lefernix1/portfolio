import React, { useState, useEffect } from "react";
import NextLink from "next/link";
import {
  Link,
  Container,
  Heading,
  Box,
  Image,
  SimpleGrid,
  Button,
  List,
  ListItem,
  Icon,
  useColorModeValue,
} from "@chakra-ui/react";
import { ChevronRightIcon } from "@chakra-ui/icons";
import Section from "../components/section";
import Paragraph from "../components/paragraph";
import { BioSection, BioYear } from "../components/bio";
import { GridItem } from "../components/grid-item";
import Layout from "../components/layouts/article";
import { IoLogoTwitter, IoLogoLinkedin, IoLogoGithub } from "react-icons/io5";

import { getSimilarPosts, getRecentPosts } from "../services/index";
import { map } from "ramda";

const Page = ({ categories, slug }) => {
  const [relatedPosts, setRelatedPosts] = useState([]);

  useEffect(() => {
    if (slug) {
      getSimilarPosts(categories, slug).then((result) => {
        setRelatedPosts(result);
      });
    } else {
      getRecentPosts().then((result) => {
        setRelatedPosts(result);
      });
    }
  }, [slug]);

  const getAge = (dateString) => {
    const ageInMilliseconds = new Date() - new Date(dateString);
    return Math.floor(ageInMilliseconds / 1000 / 60 / 60 / 24 / 365); // convert to years
  };

  return (
    <Layout>
      <Container>
        <Box
          borderRadius='lg'
          mb={6}
          p={3}
          textAlign='center'
          bg={useColorModeValue("whiteAlpha.500", "whiteAlpha.200")}
        >
          Bienvenue sur mon portfolio
        </Box>

        <Box display={{ md: "flex" }}>
          <Box flexGrow={1}>
            <Heading as='h2' variant='page-title'>
              Adrien Nombalier
            </Heading>
            <p>
              Développeur web agé de {getAge("1998/07/19")} ans travaillant en
              tant que salarié et freelance, basé en Nouvelle-Aquitaine.
            </p>
          </Box>
          <Box
            flexShrink={0}
            mt={{ base: 4, md: 0 }}
            ml={{ md: 6 }}
            textAlign='center'
          >
            <Image
              borderColor='whiteAlpha.800'
              borderWidth={2}
              borderStyle='solid'
              maxWidth='100px'
              display='inline-block'
              borderRadius='full'
              src='/images/adrien.jpeg'
              alt='Profile image'
            />
          </Box>
        </Box>

        <Section delay={0.1}>
          <Heading as='h3' variant='section-title'>
            Présentation
          </Heading>
          <Paragraph>
            Je suis un développeur full stack qui aime relever les défis et
            travailler avec des technologies de pointe. Vous pouvez voir les
            différents projets auxquels j'ai participé dans l'onglet
            <Link href='/works'> Projets</Link>, consulter mes posts sur la page
            de mon <Link href='/posts'> Blog</Link>, ou me proposer votre projet
            depuis la page <Link href='/services'> Services</Link>.
          </Paragraph>
          <Box align='center' my={4}>
            <NextLink href='/works'>
              <Button rightIcon={<ChevronRightIcon />} colorScheme='teal'>
                Mon portfolio
              </Button>
            </NextLink>
          </Box>
        </Section>

        <Section delay={0.2}>
          <Heading as='h3' variant='section-title'>
            Biographie
          </Heading>
          <BioSection>
            <BioYear>1998</BioYear>
            Né à Poitiers, en France 🇫🇷
          </BioSection>
          <BioSection>
            <BioYear>2017</BioYear>
            {"Obtention d'un baccalauréat STMG spécialité mercatique"}
          </BioSection>
          <BioSection>
            <BioYear>2018</BioYear>
            {
              "Premier pas dans le monde du developpement informatique dans la start'up"
            }{" "}
            <Link
              href='https://geovelo.fr/fr/route?bike-type=own&e-bike=false'
              target='_blank'
            >
              Géovelo
            </Link>
          </BioSection>
          <BioYear>2019</BioYear>
          Etudiant en cours à la WildCodeSchool (BAC+2)
          <BioSection>
            <BioYear>2020</BioYear>
            {
              "Etudiant en cours à la WildCodeSchool, en alternance dans la start'up F.ASS.T (BAC+4)"
            }
          </BioSection>
          <BioSection>
            <BioYear>{"2021 à aujourd'hui"}</BioYear>
            Travail en tant que développeur fullstack chez{" "}
            <Link href='https://fasst.io' target='_blank'>
              F.ASS.T
            </Link>
          </BioSection>
        </Section>

        <Section delay={0.3}>
          <Heading as='h3' variant='section-title'>
            {"J'♥"}
          </Heading>
          <Paragraph>
            Music,{" "}
            <Link href='https://illust.odoruinu.net/' target='_blank'>
              Coding,{" "}
            </Link>
            design UI/UX,{" "}
            <Link href='https://500px.com/p/craftzdog' target='_blank'>
              Sports
            </Link>
          </Paragraph>
        </Section>

        <Section delay={0.3}>
          <Heading as='h3' variant='section-title'>
            Sur internet
          </Heading>
          <List>
            <ListItem>
              <Link href='https://github.com/lefernix' target='_blank'>
                <Button
                  variant='ghost'
                  colorScheme='teal'
                  leftIcon={<Icon as={IoLogoGithub} />}
                >
                  @Lefernix
                </Button>
              </Link>
            </ListItem>
            <ListItem>
              <Link href='https://twitter.com/Iefernix' target='_blank'>
                <Button
                  variant='ghost'
                  colorScheme='teal'
                  leftIcon={<Icon as={IoLogoTwitter} />}
                >
                  @Iefernix
                </Button>
              </Link>
            </ListItem>
            <ListItem>
              <Link
                href='https://www.linkedin.com/in/adrien-nombalier'
                target='_blank'
              >
                <Button
                  variant='ghost'
                  colorScheme='teal'
                  leftIcon={<Icon as={IoLogoLinkedin} />}
                >
                  Nombalier Adrien
                </Button>
              </Link>
            </ListItem>
          </List>

          <SimpleGrid columns={[1, 2, 2]} gap={6}>
            {map(
              (recentPost) => (
                <GridItem
                  href='https://www.youtube.com/devaslife'
                  title={recentPost.title}
                  thumbnail={recentPost.featuredImage.url}
                >
                  description
                </GridItem>
              ),
              relatedPosts
            )}
          </SimpleGrid>

          <Box align='center' my={4}>
            <NextLink href='/posts'>
              <Button rightIcon={<ChevronRightIcon />} colorScheme='teal'>
                Post les plus récents
              </Button>
            </NextLink>
          </Box>
        </Section>
      </Container>
    </Layout>
  );
};

export default Page;
