import { Container, Heading } from "@chakra-ui/react";
import Layout from "../components/layouts/article";
import Course from "./service/course";

const Service = ({ posts }) => {
  return (
    <Layout title='Service'>
      <Container>
        <Heading as='h3' fontSize={20} mb={4}>
          Mes services
        </Heading>
        <Course />
      </Container>
    </Layout>
  );
};

export default Service;
