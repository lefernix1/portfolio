import {
  Container,
  Heading,
  SimpleGrid,
  Input,
  InputGroup,
  InputLeftElement,
} from "@chakra-ui/react";
import { SearchIcon } from "@chakra-ui/icons";
import Section from "../components/section";
import { PostGridItem } from "../components/grid-item";
import Layout from "../components/layouts/article";
import { getPosts } from "../services/index";

import { propOr, prop, map } from "ramda";

const Posts = ({ posts }) => {
  return (
    <Layout title='Posts'>
      <Container>
        <Heading as='h3' fontSize={20} mb={4}>
          Blog
        </Heading>
        <InputGroup>
          <InputLeftElement
            pointerEvents='none'
            children={<SearchIcon color='gray.300' />}
          />
          <Input type='tel' placeholder='Rechercher…' marginBottom={5} />
        </InputGroup>
        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          {map(
            (post) => (
              <Section>
                <PostGridItem post={post.node}></PostGridItem>
              </Section>
            ),
            posts
          )}
        </SimpleGrid>
      </Container>
    </Layout>
  );
};

export default Posts;

export async function getStaticProps() {
  const posts = (await getPosts()) || [];

  return {
    props: { posts },
  };
}
