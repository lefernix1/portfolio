import React from "react";
import {
  Heading,
  Box,
  Image,
  Flex,
  useColorModeValue,
  Badge,
} from "@chakra-ui/react";

import moment from "moment";

export const Author = ({ author, createdAt, updatedAt }) => {
  return (
    <Flex
      borderRadius='lg'
      m={6}
      p={3}
      bg={useColorModeValue("whiteAlpha.500", "whiteAlpha.200")}
    >
      <Image
        borderColor='whiteAlpha.800'
        borderWidth={2}
        borderStyle='solid'
        w='120px'
        h='120px'
        mr='4'
        display='inline-block'
        borderRadius='full'
        objectFit='cover'
        src={author.photo.url}
        alt='Profile image'
      />
      <Box flexGrow={1}>
        <Heading as='h2' variant='page-title'>
          {author.name}
        </Heading>
        <p>{author.bio}</p>
        <Box display='flex' flexDirection='column'>
          <Badge mt={2} width='fit-content'>
            Publié le : {moment(createdAt).format("DD/MM/YYYY")}
          </Badge>
          <Badge mt={2} width='fit-content'>
            Mis à jour le : {moment(updatedAt).format("DD/MM/YYYY")}
          </Badge>
        </Box>
      </Box>
    </Flex>
  );
};

export default Author;
