import NextLink from 'next/link'
import Image from 'next/image'
import { Box, Text, LinkBox, LinkOverlay, Tag } from '@chakra-ui/react'
import { Global } from '@emotion/react'
import { map } from 'ramda'

export const GridItem = ({ children, href, title, thumbnail }) => (
    <Box w="100%" textAlign="center">
        <LinkBox cursor="pointer">
            <Image
                src={thumbnail}
                alt={title}
                className="grid-item-thumbnail"
                loading="lazy"
                width="300px"
                height="200px"
            />
            <LinkOverlay href={href} target="_blank">
                <Text mt={2}>{title}</Text>
            </LinkOverlay>
            <Text fontSize={14}>{children}</Text>
        </LinkBox>
    </Box>
)

export const WorkGridItem = ({ children, id, title, thumbnail }) => (
    <Box w="100%">
        <NextLink href={`/works/${id}`}>
            <LinkBox cursor="pointer">
                <Image
                    src={thumbnail}
                    alt={title}
                    className="grid-item-thumbnail"
                    placeholder="blur"
                />
                <LinkOverlay href={`/works/${id}`}>
                    <Text mt={2} fontSize={20}>
                        {title}
                    </Text>
                </LinkOverlay>
                <Text fontSize={14}>{children}</Text>
            </LinkBox>
        </NextLink>
    </Box>
)

export const PostGridItem = ({  post }) => (
    <Box w="100%">
        <NextLink href={`/posts/${post.slug}`}>
            <LinkBox cursor="pointer">
                <Image
                    src={post.featuredImage.url}
                    alt={"image"}
                    className="grid-item-thumbnail"
                    width="300px"
                    height="200px"
                />
                <LinkOverlay href={`/posts/${post.slug}`}>
                    <Text mt={2} fontSize={20}>
                        {post.title}
                    </Text>
                    {map(category =>
                    <Tag colorScheme="yellow">
                        {category.name}
                    </Tag>
                    ,post.categories)}
                </LinkOverlay>
                {/* <Text fontSize={14}>{children}</Text> */}
            </LinkBox>
        </NextLink>
    </Box>
)

export const GridItemStyle = () => (
    <Global
        styles={`
      .grid-item-thumbnail {
        border-radius: 12px;
      }
    `}
    />
)